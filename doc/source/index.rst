Interop Working Group Documentation
===================================

This Committee was formed during the OpenStack Ice House Summit in Hong Kong
by Board Resolution on 11/4.

Interop Working Group sets base requirements by defining:

1. capabilities,
2. code and
3. must-pass tests for all OpenStack products.

This definition uses community resources and involvement to drive
interoperability by creating the minimum standards for products labeled
"OpenStack."

Our mission is to define "OpenStack Core" that is supported by all
implementations as chartered by the by-laws.

Tools for Interoperability Testing
----------------------------------

* `refstack <https://docs.opendev.org/openinfra/refstack/latest/>`_
  is a user interface to display individual test run results, it is deployed
  `at this page <https://refstack.openstack.org/#/>`_.
* `refstack-client <https://docs.opendev.org/openinfra/refstack-client/latest/>`_
  is a command line utility that allows user to execute Tempest test runs based
  on the specified configuration. When finished running Tempest, it can send
  the test results to the RefStack server.

Content:
--------

.. toctree::
   :maxdepth: 2

   process/index
   schema/index
   guidelines/index
   contributing
   hacking
