=======================
Guideline Documentation
=======================

Latest Guideline
----------------
.. toctree::
   :maxdepth: 1
   :includehidden:

   all.2022.11


Proposed Guideline
------------------

.. toctree::
   :maxdepth: 1
   :includehidden:

   all.next


Previous Guidelines
-------------------

.. toctree::
   :maxdepth: 1
   :includehidden:

   all.2022.06
   all.2021.11
   all.2020.11
   all.2020.06
   all.2019.11
   all.2019.06
   all.2018.11
   2018.02
   2017.09
   2017.01
   2016.08
   2016.01
   2015.07
   2015.05
   2015.04
   2015.03
