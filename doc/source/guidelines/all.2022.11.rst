============================================
OpenStack Interoperability Guideline 2022.11
============================================

:Status: draft
:Replaces: 2022.06
:JSON Master: https://opendev.org/openinfra/interop/raw/branch/master/guidelines/2022.11.json

This document outlines the mandatory capabilities and designated
sections required to exist in a software installation in order to
be eligible to use marks controlled by the OpenStack Foundation.

This document was generated from the `<2022.11.json>`_.

Releases Covered
==============================
Applies to Xena, Yoga, Zed, Antelope





OS_powered_compute Component Capabilities
=========================================
Required Capabilities
-----------------------
* compute-availability-zones-list (Nova)
* compute-flavors-list (Nova)
* compute-instance-actions-get (Nova)
* compute-instance-actions-list (Nova)
* compute-keypairs-create (Nova)
* compute-keypairs-create-type (Nova)
* compute-list-api-versions (Nova)
* compute-quotas-get (Nova)
* compute-servers-create (Nova)
* compute-servers-create-multiple (Nova)
* compute-servers-delete (Nova)
* compute-servers-get (Nova)
* compute-servers-host (Nova)
* compute-servers-invalid (Nova)
* compute-servers-list (Nova)
* compute-servers-lock (Nova)
* compute-servers-name (Nova)
* compute-servers-reboot (Nova)
* compute-servers-rebuild (Nova)
* compute-servers-stop (Nova)
* compute-servers-update (Nova)
* compute-servers-verify (Nova)
* compute-volume-attach (Nova)
* compute-images-create (Nova)
* compute-servers-resize (Nova)
* compute-servers-tags-CRUD (Nova)
* compute-servers-tags-tag-CRUD (Nova)
* identity-v3-api-discovery (Keystone)
* identity-v3-catalog (Keystone)
* identity-v3-list-projects (Keystone)
* identity-v3-tokens-create (Keystone)
* identity-v3-tokens-delete (Keystone)
* identity-v3-tokens-validate (Keystone)
* identity-v3-list-versions (Keystone)
* identity-v3-application-credentials (Keystone)
* identity-v3-users (Keystone)
* identity-v3-domains (Keystone)
* images-v2-index (Glance)
* images-v2-update (Glance)
* images-v2-list (Glance)
* images-v2-delete (Glance)
* images-v2-get (Glance)
* images-v2-import (Glance)
* networks-l2-CRUD (Neutron)
* networks-l3-router (Neutron)
* networks-l3-CRUD (Neutron)
* networks-list-api-versions (Neutron)
* networks-security-groups-CRUD (Neutron)
* networks-subnet-pools-CRUD (Neutron)
* networks-security-groups-CRUD-2 (Neutron)
* networks-dhcp (Neutron)
* networks-floating-ips-CRUD (Neutron)
* volumes-list-api-versions (Cinder)
* volumes-v3-create-delete (Cinder)
* volumes-v3-snapshot-create-delete (Cinder)
* volumes-v3-get (Cinder)
* volumes-v3-list (Cinder)
* volumes-v3-update (Cinder)
* volumes-v3-copy-image-to-volume (Cinder)
* volumes-v3-clone (Cinder)
* volumes-v3-availability-zones (Cinder)
* volumes-v3-extensions (Cinder)
* volumes-v3-metadata (Cinder)
* volumes-v3-readonly (Cinder)
* volumes-v3-upload (Cinder)
* volumes-v3-snapshots-list (Cinder)

Advisory Capabilities
-----------------------
* networks-address-pairs (Neutron)
* networks-extensions (Neutron)
* networks-bulk (Neutron)
* networks-negative (Neutron)
* networks-ipv6 (Neutron)
* networks-show-api-details (Neutron)
* networks-port-list-filter (Neutron)

Deprecated Capabilities
-------------------------
* compute-servers-metadata-delete (Nova)
* compute-servers-metadata-get (Nova)
* compute-servers-metadata-list (Nova)
* compute-servers-metadata-set (Nova)
* compute-servers-metadata-update (Nova)

Removed Capabilities
----------------------
None




OS_powered_storage Component Capabilities
=========================================
Required Capabilities
-----------------------
* objectstore-account-quotas (Swift)
* objectstore-account-list (Swift)
* objectstore-container-acl (Swift)
* objectstore-container-quotas (Swift)
* objectstore-container-create (Swift)
* objectstore-container-delete (Swift)
* objectstore-container-list (Swift)
* objectstore-container-metadata (Swift)
* objectstore-dlo-support (Swift)
* objectstore-slo-support (Swift)
* objectstore-info-request (Swift)
* objectstore-object-copy (Swift)
* objectstore-object-create (Swift)
* objectstore-object-delete (Swift)
* objectstore-object-get (Swift)
* objectstore-object-versioned (Swift)
* objectstore-temp-url-get (Swift)
* objectstore-temp-url-put (Swift)
* objectstore-account-update (Swift)
* objectstore-bulk (Swift)
* identity-v3-tokens-create (Keystone)
* identity-v3-tokens-delete (Keystone)

Advisory Capabilities
-----------------------
None

Deprecated Capabilities
-------------------------
None

Removed Capabilities
----------------------
None


Designated Sections
=====================================

The following designated sections apply to the same releases as
this specification.

Required Designated Sections
----------------------------

* Cinder : Designated sections are the API implementation code
* Glance : Designated sections are the API implementation code and domain
  model.
* Keystone : Designation is outlined per API grouping. Identity (user and
  group) management APIs will not be designated. API access (with exception of
  auth) may be prohibited by policy (resulting in HTTP 403). Designated APIs
  include v3 version where applicable.
* Neutron : By default, designated for all code backing required capabilities
  except pluggable components such as plugins, drivers, and API extensions
  other than those listed below.
* Nova : By default, designated except scheduler, filter, drivers, API
  extensions and networking. Additional properties on responses are not
  allowed.
* Swift : Designated sections are proxy server, object server, container
  server, account server and select middleware

Advisory Designated Sections
----------------------------

None

Deprecated Designated Sections
------------------------------

None

Removed Designated Sections
---------------------------

None
================================================
OpenStack Interoperability Guideline dns.2022.11
================================================

:Status: draft
:Replaces: dns.2022.06
:JSON Master: https://opendev.org/openinfra/interop/raw/branch/master/add-ons/guidelines/dns.2022.11.json

This document outlines the mandatory capabilities and designated
sections required to exist in a software installation in order to
be eligible to use marks controlled by the OpenStack Foundation.

This document was generated from the `<dns.2022.11.json>`_.

Releases Covered
==============================
Applies to Xena, Yoga, Zed, Antelope





OS_powered_dns Component Capabilities
=====================================
Required Capabilities
-----------------------
* dns-record-crud (Designate)
* dns-zone-crud (Designate)

Advisory Capabilities
-----------------------
None

Deprecated Capabilities
-------------------------
None

Removed Capabilities
----------------------
None


Designated Sections
=====================================

The following designated sections apply to the same releases as
this specification.

Required Designated Sections
----------------------------

* Designate : API and Central code, allowing for plugins and excluding storage
  and scheduler filters

Advisory Designated Sections
----------------------------

None

Deprecated Designated Sections
------------------------------

None

Removed Designated Sections
---------------------------

None
========================================================
OpenStack Interoperability Guideline key_manager.2022.11
========================================================

:Status: draft
:Replaces:
:JSON Master: https://opendev.org/openinfra/interop/raw/branch/master/add-ons/guidelines/key_manager.2022.11.json

This document outlines the mandatory capabilities and designated
sections required to exist in a software installation in order to
be eligible to use marks controlled by the OpenStack Foundation.

This document was generated from the `<key_manager.2022.11.json>`_.

Releases Covered
==============================
Applies to Xena, Yoga, Zed, Antelope





OS_powered_key_manager Component Capabilities
=============================================
Required Capabilities
-----------------------
None

Advisory Capabilities
-----------------------
* secret-CRUD (Barbican)
* secret-generation (Barbican)
* secret-management (Barbican)
* system-management (Barbican)

Deprecated Capabilities
-------------------------
None

Removed Capabilities
----------------------
None


Designated Sections
=====================================

The following designated sections apply to the same releases as
this specification.

Required Designated Sections
----------------------------

* Barbican : Key Manager solution.

Advisory Designated Sections
----------------------------

None

Deprecated Designated Sections
------------------------------

None

Removed Designated Sections
---------------------------

None
==========================================================
OpenStack Interoperability Guideline load_balancer.2022.11
==========================================================

:Status: draft
:Replaces:
:JSON Master: https://opendev.org/openinfra/interop/raw/branch/master/add-ons/guidelines/load_balancer.2022.11.json

This document outlines the mandatory capabilities and designated
sections required to exist in a software installation in order to
be eligible to use marks controlled by the OpenStack Foundation.

This document was generated from the `<load_balancer.2022.11.json>`_.

Releases Covered
==============================
Applies to Xena, Yoga, Zed, Antelope





OS_powered_load_balancer Component Capabilities
===============================================
Required Capabilities
-----------------------
None

Advisory Capabilities
-----------------------
* provider-list (Octavia)
* load-balancer-CRUD (Octavia)
* create-listener (Octavia)
* show-listener (Octavia)
* show-listener-stats (Octavia)
* list-listeners (Octavia)
* update-listener (Octavia)
* delete-listener (Octavia)

Deprecated Capabilities
-------------------------
None

Removed Capabilities
----------------------
None


Designated Sections
=====================================

The following designated sections apply to the same releases as
this specification.

Required Designated Sections
----------------------------

* Octavia : Load Balancing solution.

Advisory Designated Sections
----------------------------

None

Deprecated Designated Sections
------------------------------

None

Removed Designated Sections
---------------------------

None
==========================================================
OpenStack Interoperability Guideline orchestration.2022.11
==========================================================

:Status: draft
:Replaces: orchestration.2022.06
:JSON Master: https://opendev.org/openinfra/interop/raw/branch/master/add-ons/guidelines/orchestration.2022.11.json

This document outlines the mandatory capabilities and designated
sections required to exist in a software installation in order to
be eligible to use marks controlled by the OpenStack Foundation.

This document was generated from the `<orchestration.2022.11.json>`_.

Releases Covered
==============================
Applies to Xena, Yoga, Zed, Antelope





OS_powered_orchestration Component Capabilities
===============================================
Required Capabilities
-----------------------
* stack-create (Heat)
* stack-delete (Heat)
* stack-environment-parameter (Heat)
* stack-event (Heat)
* stack-hook (Heat)
* stack-list (Heat)
* stack-output (Heat)
* stack-preview (Heat)
* stack-list-resources (Heat)
* stack-list-resource-types (Heat)
* stack-list-template-functions (Heat)
* stack-list-template-versions (Heat)
* stack-mark-resource-unhealthy (Heat)
* stack-resource-type-template (Heat)
* stack-show (Heat)
* stack-show-resource (Heat)
* stack-show-resource-type (Heat)
* stack-show-unhealthy-resource (Heat)
* stack-signal-resource (Heat)
* stack-software-config (Heat)
* stack-tag (Heat)
* stack-template (Heat)
* stack-update (Heat)

Advisory Capabilities
-----------------------
None

Deprecated Capabilities
-------------------------
None

Removed Capabilities
----------------------
None


Designated Sections
=====================================

The following designated sections apply to the same releases as
this specification.

Required Designated Sections
----------------------------

* Heat : Must at a minimum include resources for OpenStack Powered Compute
  components.

Advisory Designated Sections
----------------------------

None

Deprecated Designated Sections
------------------------------

None

Removed Designated Sections
---------------------------

None
===============================================================
OpenStack Interoperability Guideline shared_file_system.2022.11
===============================================================

:Status: draft
:Replaces: shared_file_system.2022.06
:JSON Master: https://opendev.org/openinfra/interop/raw/branch/master/add-ons/guidelines/shared_file_system.2022.11.json

This document outlines the mandatory capabilities and designated
sections required to exist in a software installation in order to
be eligible to use marks controlled by the OpenStack Foundation.

This document was generated from the `<shared_file_system.2022.11.json>`_.

Releases Covered
==============================
Applies to Xena, Yoga, Zed, Antelope





OS_powered_shared_file_system Component Capabilities
====================================================
Required Capabilities
-----------------------
* share-list-api-versions (Manila)
* share-create-delete (Manila)
* share-get (Manila)
* share-list (Manila)
* share-update (Manila)
* share-extend (Manila)
* share-shrink (Manila)
* share-network-create-delete (Manila)
* share-network-update (Manila)
* share-network-get (Manila)
* share-network-list (Manila)

Advisory Capabilities
-----------------------
None

Deprecated Capabilities
-------------------------
None

Removed Capabilities
----------------------
None


Designated Sections
=====================================

The following designated sections apply to the same releases as
this specification.

Required Designated Sections
----------------------------

* Manila : The Shared File Systems v2 API (with microversions) is designated

Advisory Designated Sections
----------------------------

None

Deprecated Designated Sections
------------------------------

None

Removed Designated Sections
---------------------------

None
