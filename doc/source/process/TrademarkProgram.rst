============================
Trademark Process Definition
============================

.. replaces CoreDefintion.rst.

Objective
=========

The following list represents the "guiding principles" used by the
Foundation Board to determine how commercial implementations of OpenStack
can be granted use of the trademark.

::

   Principles Adopted at Oct 4th 2013 Board Meeting

Implementation
==============

* The `Governance/InteropWG
  <https://wiki.openstack.org/wiki/Governance/InteropWG>`_ is
  working to manage this.
* Meetings and agendas are linked from that page and open to the community.

Principles
==========

.. image:: ../images/500px-Core_flow.png

1. Implementations that are Required Cloud Services can use OpenStack
Trademark (OpenStack™)

   1. This is the legal definition of "core" and the why it matters to the
      community.

   2. We want to make sure that the OpenStack™ mark means something.

   3. The OpenStack™ mark is not the same as the OpenStack brand; however,
      the Board uses its control of the mark as a proxy to help manage the
      brand.

2. Required Cloud Services is a subset of the whole project

   1. OpenStack is a broad and diverse community with growing functionality.
      This growing functionality is achieved via "projects" that expose
      services to create Cloud Computing Platforms. This constant innovation
      is vital to OpenStack. The pursuit of this Interop effort is to define
      a common stable subset OpenStack functionality that most cloud platform
      are using. Not all services, and not all features of chosen services
      are required to do that. For OpenStack Logo Program The Open
      Infrastructure Foundation defines what projects are required services.
      Currently, the list of services consists of:
      Nova, Keystone, Neutron, Cinder, Glance and Swift. See
      `OpenStack Software page
      <https://www.openstack.org/software/project-navigator/openstack-components/#openstack-services>`_.

   2. The Interop effort is currently centered around three Platform programs:

      - OpenStack Powered Platform,
      - OpenStack Powered Compute and
      - OpenStack Powered Storage.

      Each of these programs have designated sections of OpenStack components
      that form an important part of interoperability across implementations.
      Alongside these platforms, there are also "add-on" services that extend
      the functionality. OpenStack Powered Compute Platform
      encompasses designated sections from:

      - OpenStack Identity (keystone),
      - OpenStack Compute (nova),
      - OpenStack Image Storage (glance),
      - OpenStack Block Storage (cinder) and
      - OpenStack Networking (neutron) services.

      The separate add-on guidelines include designated sections of:

      - OpenStack DNS (designate),
      - OpenStack Orchestration (heat) and
      - OpenStack Shared File System Storage (manila) services, respectively.

   3. There are other Add-on Trademarks that are managed together with
      the Required Cloud Services by the Open Infrastructure Foundation,
      and available for the platform  ecosystem as per
      the Board’s discretion, and administered by Interop WG.

   4. Currently there are three Add-on programs: OpenStack with DNS,
      OpenStack with Orchestration, and OpenStack with Shared File System.
      These three add Designate, Heat and Manila projects to the Openstack
      Powered programs.

   5. "OpenStack API Compatible" Trademark is not part of this discussion and
      should be not be assumed.

3.  Required Cloud Services and Add-on definitions can be applied equally
to all usage models

   1. There should not be multiple definitions of OpenStack depending on
      the operator (public, private, community, etc)

   2. While expected that each deployment is identical, the differences
      must be quantifiable

4. Claiming OpenStack requiring use of designated upstream code

   1. Implementations claiming the OpenStack™ Trademark must use the OpenStack
      upstream code (or be using code submitted to upstream)

   2. You are not OpenStack, if you pass all the tests but do not use the
      API framework

   3. This also surfaces bit-rot in alternate implementations to the larger
      community

   4. This behavior improves interoperability because there is more shared
      code between implementations

5. Projects must have an open reference implementation

   1. OpenStack will require an open source reference base plug-in
      implementation for projects (if not part of OpenStack, license model
      for reference plug-in must be compatible).

   2. Definition of a plug-in: alternate backend implementations with a
      common API framework that uses common _code_ to implement the API.
      That is commonly referred to as a driver.

   3. This expects that projects (where technically feasible) are expected
      to implement a plug-in or extension architecture.

   4. This is already in place for several projects and addresses around
      ecosystem support, enabling innovation.

   5. Reference plug-ins are, by definition, the complete capability set.
      It is not acceptable to have "core" features that are not functional
      in the reference plug-in.

   6. This will enable alternate implementations to offer innovative or
      differentiated features without forcing changes to the reference
      plug-in implementation. These are commonly referred to as vendor
      drivers.

   7. This will enable the reference to expand without forcing other
      alternate implementations to match all features and recertify.

6. Vendors may utilize vendor plug-ins as alternative implementations
to reference plug-ins

   1. If a vendor plug-in passes all relevant tests then it can be
      considered a full substitute for the reference plug-in

   2. If a vendor plug-in does NOT pass all relevant test then the vendor
      is required to include the open source reference in the
      implementation.

   3. Vendor plug-in implementations may pass any tests that make sense

   4. Vendor plug-in implementations should add tests to validate new
      functionality.

   5. They must have all the must-pass tests (see #10) to claim the
      OpenStack Trademark.

   6. OpenStack Implementations are verified by open community tests

   7. Vendor OpenStack implementations must achieve 100% of must-have
      coverage?

   8. Implemented tests can be flagged as may-have requires list.

   9. Certifiers will be required to disclose their testing gaps.

   10. This will put a lot of pressure on the Tempest project.

   11. Maintenance of the testing suite to become a core Open
       Infrastructure Foundation responsibility.
       This may require additional resources.

   12. Implementations and products are allowed to have variation based on
       publication of compatibility.

   13. Consumers must have a way to determine how the system is different
       from reference (posted, discovered, etc.)

   14. Testing must respond in an appropriate way on BOTH pass and fail
       (the wrong return rejects the entire suite)

   15. Vendor plug-in implementations are applicable to all projects
       under Interop programs, both Required Cloud Services and Add-ons.

7. Tests can be remotely or self-administered

   1. Plug-in certification is driven by Tempest self-certification model

   2. Self-certifiers are required to publish their results

   3. Self-certified are required to publish enough information that a 3rd
      party could build the reference implementation to pass the tests.

   4. Self-certified must include the operating systems that have been
      certified

   5. It is preferred for self-certified implementation to reference an
      OpenStack reference architecture "flavor" instead of defining their
      own reference. (a way to publish and agree on flavors is needed)

   6. The Open Infrastructure Foundation had defined a mechanism of
      dispute resolution. (A trust but verify model)

   7. As an ecosystem partner, you have a need to make a "works against
      OpenStack" statement that is supportable

   8. API consumer can claim working against the OpenStack API if it works
      against any implementation passing all the "must have" tests(YES)

   9. API consumers can state they are working against the OpenStack API
      with some "may have" items as requirements

   10. API consumers are expected to write tests that validate their
       required behaviors (submitted as "may have" tests)

8. A subset of tests are chosen by the Open Infrastructure Foundation
as "must-pass"

   1. How? Read the `Governance/CoreCriteria <./CoreCriteria.rst/>`_ Selection
      Process

   2. An OpenStack body will recommend which tests are elevated from
      may-have to must-have

   3. The selection of "must-pass" tests should be based on quantifiable
      information when possible.

   4. Must-pass tests should be selected from the existing body of
      "may-pass" tests. This encourages people to write tests for cases
      they want supported.

   5. We will have a process by which tests are elevated from may to must
      lists

   6. Potentially: the User Committee will nominate tests that elevated to
      the board

   7. OpenStack Powered Trademark means passing all "must-pass" tests

9. The OpenStack board delegated to Interop WG responsibility
to define Trademark criteria – to approve 'musts'.

   1. The Interop WG will submit the must-pass tests to the Approval Committee
      as a block and passed as a single motion.

   2. We are NOT defining which items are on the list in this effort, just
      making the position that it is how we will define Required Cloud
      Services.

   3. May-have tests include items in the integrated release, but which are
      not core.

   4. Must haves – must comply with the Core criteria defined from the
      IncUp committee results

   5. Interop WG can propose new Add-on programs for inclusion for OpenStack
      Powered Trademark to the Approval Committee.

   6. Interop WG must bring to the Open Infrastructure Foundation any major
      changes to OpenStack Trademark program for approval. Approval of new
      guidelines, adding new projects to Add-on Trademark are not considered
      major change to the operation of the OpenStack Trademark program. These
      are handled by the Approval Committee. Process changes,
      like the membership of the Approval Committee,
      alignment of OpenStack Powered Logo to OpenStack
      TC changes to grouping of OpenStack projects into use case scenarios
      are examples of major changes that require the Open Infrastructure
      Board approval.

10. OpenStack Trademark means passing all "must-pass" tests

    1. The Approval Committee owns the responsibility
       to define 'guidelines' - to approve 'musts'

    2. We are NOT defining which items are on the list in this effort, just
       making the position that it is how we will define guideline

    3. May-have tests include items in the release, but which
       are not core functionality of included projects.

    4. Must haves – must comply with the criteria defined in 'guidelines' from
       the committee results

    5. Projects that are not included in 'the Required Cloud Services'
       or 'Add-on' programs
       are not to be included in the 'may' or 'must' list
