Governance Process
==================

* Meetings details and agendas are available in
  `the etherpad page <https://etherpad.opendev.org/p/interop>`_ and are open
  to the community.

* Meeting info is available at:
  https://meetings.opendev.org/#Interop_Working_Group_Meeting

* Members are expected to do their homework. We will not be rehashing
  due to time limits. Minutes from prior meetings are available in
  `the etherpad page <https://etherpad.opendev.org/p/interop>`_.
