Internal Process Documentation
==============================

Here we document all internal (within the IWG group) actions needed in order
to release a new guideline.
This documentation should help to avoid mistakes and to ensure that every
guideline is released in a standardized way.

This document is based on
`OpenStack Interop Working Group Process 2021A <./2021A.rst/>`_ although it
focuses more on the internal (within the IWG group) actions, such as order of
reviews, their content and timing.


Actions done during a cycle
---------------------------
This section summarizes actions in a suggested order IWG team should focus on
during the cycle.

* See if we have any new tests (for projects currently covered by Interop) in
  tempest which haven't been added to interop yet, (this script will help here
  `<https://review.opendev.org/c/osf/interop/+/799201>`_)

  * If there are new tests, decide whether they are worth to be included in any
    (maybe new) capability.

    * If yes, add them to a capability / create a new one.

      * when creating a new capability make sure that required_since is empty -
        it will be set once the capability gets under required (new ones are
        under advisory)

    * If not, add them to the exclude list with a brief explanation why
      so that the test won't come up in a future release.

* See if we have any new deprecated tests in Tempest or if any tests got
  removed from Tempest recently (only tests related to the projects under
  Interop coverage concern us). E.g. go through merged patches within the
  relevant time frame.

  * If there are any deprecated tests, let's deprecate the capabilities which
    contain the deprecated tests (move them from required to deprecated in the
    relevant next files). Don't forget to **mention a related link** in the
    commit message (f.e. tempest change which deprecated the tests).

  * If there are any removed tests, verify whether the related capabilities
    were deprecated already.

    * If they were, consider moving the capabilities to removed section.

    * If they were not, deprecate them. They will be removed in the next cycle.

* Watch out for any reported issues or skips (f.e. in the upstream CI) of tests
  which are part of any capability which is currently under advisory. This info
  will be crucial in the release process when we decide whether to move the
  advisory capabilities to the required section.

Release process (starting 1 month before PTG)
---------------------------------------------

Changes to be done within next.json files are documented in this section.

1. Make sure tempest/<plugin> sha is correct
   (test_repositories.tempest/<plugin>.reference)
2. Make sure values listed under os_trademark_approval are correct
   (target_approval, replaces, releases)
3. Make sure required_since within lately added tests is equal to an empty
   string ("") the guideline release and to the target_approval
4. Verify all flagged tests should be still flagged - unflag them otherwise
5. Verify all advisory tests we still want to be advisory, if not move them
   accordingly

  1. If you move a test from advisory (probably to required), set
     required_since to the guideline release and to the target_approval

6. Check if any of the capabilities are deprecated.
7. Repeat the above process for every add-on's next file
8. Rename next.json to <target_release.json> - in case there are other changes
   in the next.json file, do this step in a separate patch as the rename appear
   in gerrit as a new file which makes it harder to see other changes within
   the file.

  1. Edit required_platform_components.source references in the add-on next
     files so that they point to the new platform guideline being released
     (next.json -> <target_release>.json)

9. Copy and rename the newly created <target_release>.json files to next.json
   files (<add-on>.next.json)

 1. Run the following script to update the documentation for next and
    target_release guidelines. It will generate a new file named
    *all.<target_release>.rst* and update *all.next.rst* under
    *doc/source/guidelines/*

 .. code-block:: console

   $ tools/jsonToRst.py --all next
   $ tools/jsonToRst.py --all <target_release>

  2. Edit *doc/source/guidelines/index.rst* file to make the new guideline doc
     visible - move the latest guideline from **Latest Guideline** section to
     the **Previous Guidelines** section and put the new guideline to the
     **Latest Guideline** section.

  3. Edit the symlinks so that they point to the latest guidelines:

  .. code-block:: console

    $ rm current_guideline
    $ ln -s guidelines/<target_release>.json current_guideline
    $ cd add-ons/
    $ rm *_current_guideline
    $ ln -s guidelines/dns.<target_release>.json dns_current_guideline
    $ ln -s guidelines/key_manager.<target_release>.json key_manager_current_guideline
    $ ln -s guidelines/load_balancer.<target_release>.json load_balancer_current_guideline
    $ ln -s guidelines/orchestration.<target_release>.json orchestration_current_guideline
    $ ln -s guidelines/shared_file_system.<target_release>.json shared_file_system_current_guideline

10. Add PTL or project representatives for each project that the changed files
    are covering, to review the changes.
11. Add Foundation Marketplace representatives to review the proposed
    guidelines.
12. Change status from draft to approved. (after committee review - that will
    be in separate patch) -> ~ 3 months after PTG

  1. Run the following script to reflect the status change in the documentation

  .. code-block:: console

    $ tools/jsonToRst.py --all <target_release>
