====================
Schema Documentation
====================

Latest Schema
-------------

.. toctree::
   :maxdepth: 1
   :includehidden:

   2.0.rst


Previous Schemas
----------------

.. toctree::
   :maxdepth: 2
   :includehidden:

   1.6.rst
   1.5.rst
   1.4.rst
   1.3.rst
   1.2.rst
