The source repository for this project can be found at:

    https://opendev.org/openinfra/interop

To start contributing to OpenStack, follow the steps in the contribution guide
to set up and use Gerrit:

    https://docs.openstack.org/contributors/code-and-documentation/quick-start.html

Documentation of the project can be found at:

    https://docs.opendev.org/openinfra/interop/latest/

Bugs should be filed on Storyboard:

    https://storyboard.openstack.org/#!/project/openinfra/interop

Patches against this project can be found at:

    https://review.opendev.org/q/project:openinfra/interop

To communicate with us you may use one of the following means:

**Mailing List:**
Get in touch with us via `email <mailto:openstack-discuss@lists.openstack.org>`_.
Use [interop] in your subject.

**IRC:**
We're at #openstack-interop channel on OFTC network.
`Setup IRC <https://docs.openstack.org/contributors/common/irc.html>`_

**Meetings:**
`Visit this link <https://meetings.opendev.org/#Interop_Working_Group_Meeting>`_
for the meeting information.
