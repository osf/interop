=============================================
Understanding the Interoperability Guidelines
=============================================

This repository contains files managed by the Interop Working
Group that provide guidance for the OpenStack community.

NOTE: Changes to file requires approval of the Interop Working
Group chair(s).


Interop Working Group Process Documentation
============================================

Are you a vendor who wants to get a license to use the OpenStack trademark
and logo?  Consult `OpenStack Interop
<https://www.openstack.org/brand/interop/>`_.

The `Process Documentation <https://docs.opendev.org/openinfra/interop/latest/process/index.html>`_
page contains details about theInterop Working Group process.

The `Schema Documentation <https://docs.opendev.org/openinfra/interop/latest/schema/index.html>`_
page contains details about the JSON schema versions used to express Guidelines.

The `Guideline Documentation <https://docs.opendev.org/openinfra/interop/latest/guidelines/index.html>`_
page contains RST versions of the Interop Guidelines approved by the OpenStack
Board of Directors.

**Another Useful Resources**
`Core Definition <https://docs.opendev.org/openinfra/interop/latest/process/CoreDefinition.html>`_
`Process Governance <https://docs.opendev.org/openinfra/interop/latest/process/2021A.html>`_
`Designated Sections <https://docs.opendev.org/openinfra/interop/latest/process/DesignatedSections.html>`_
`Interop WG Governance <https://docs.opendev.org/openinfra/interop/latest/process/GovernanceProcess.html>`_
`Terminology <https://docs.opendev.org/openinfra/interop/latest/process/Lexicon.html>`_

Tools for Interoperability Testing
==================================
`refstack client <https://docs.opendev.org/openinfra/refstack-client/latest/>`_
contains the tools you will need to run the Interop Working Group tests.

`refstack server <https://refstack.openstack.org>`_ is our API to collect
interoperability test results from private and public cloud vendors.
`See more <https://docs.opendev.org/openinfra/refstack/latest/>`_ about the
project.

Get Involved!
=============

See the `CONTRIBUTING <https://docs.opendev.org/openinfra/interop/latest/contributing.html>`_
guide on how to get involved.
