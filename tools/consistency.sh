#!/bin/bash

# This script will run consistency checks for Tempest tests against
# the current and next interoperability guidelines. It can run in two
# modes.
#
# * If no arguments are specified, the script will check out Tempest and
#   tempest plugins into a temporary directory, run the consistency checks,
#   then delete temporary checkout.
#
# * If an argument is given, this script will assume that it is a
#   user checked-out repository and run the consistency checks against
#   that, and leave the directory unchanged on exit. This mode is useful
#   for gate jobs and Tempest/tempest plugin development.

set -x

UPPER_CONSTRAINTS_FILE="https://opendev.org/openstack/requirements/raw/branch/master/upper-constraints.txt"

# Prints help
function usage {
    SCRIPT_NAME="basename ${BASH_SOURCE[0]}"
    echo "Usage: ${SCRIPT_NAME} [OPTION]..."
    echo "Consistency check"
    echo ""
    echo "  -h  Print this usage message"
    echo "  -t  Local Tempest directory"
    echo "  -d  Local designate-tempest-plugin directory"
    echo "  -o  Local heat-tempest-plugin directory"
    echo "  -s  Local manila-tempest-plugin directory"
    echo "  -c  Set if tempest and plugins directory should be removed after"
    echo "      the consistency check"
    exit 1
}

while getopts t:d:o:s:ch FLAG; do
    case ${FLAG} in
        t)
            TEMPESTDIR=${OPTARG}
            ;;
        d)
            DNSDIR=${OPTARG}
            ;;
        o)
            ORCHESTRATIONDIR=${OPTARG}
            ;;
        s)
            SFSDIR=${OPTARG}
            ;;
        c)
            CLEANTEMPEST=true
            ;;
        h)  #show help
            usage
            ;;
        \?) #unrecognized option - show help
            echo -e \\n"Option -$OPTARG not allowed."
            usage
            ;;
    esac
done

# check if a local directory was given (for Tempest or one of the plugins),
# if not, create a temp dir and clone the project there
if [[ -z $TEMPESTDIR ]]; then
    TEMPESTDIR=$(mktemp -d)
    git clone https://opendev.org/openstack/tempest $TEMPESTDIR
fi
if [[ -z $DNSDIR ]]; then
    DNSDIR=$(mktemp -d)
    git clone https://opendev.org/openstack/designate-tempest-plugin $DNSDIR
fi
if [[ -z $ORCHESTRATIONDIR ]]; then
    ORCHESTRATIONDIR=$(mktemp -d)
    git clone https://opendev.org/openstack/heat-tempest-plugin $ORCHESTRATIONDIR
fi
if [[ -z $SFSDIR ]]; then
    SFSDIR=$(mktemp -d)
    git clone https://opendev.org/openstack/manila-tempest-plugin $SFSDIR
fi

pip install -c ${UPPER_CONSTRAINTS_FILE} $TEMPESTDIR
pip install -c ${UPPER_CONSTRAINTS_FILE} $DNSDIR
pip install -c ${UPPER_CONSTRAINTS_FILE} $ORCHESTRATIONDIR
pip install -c ${UPPER_CONSTRAINTS_FILE} $SFSDIR

export PYTHONPATH=$TEMPESTDIR:$DNSDIR:$ORCHESTRATIONDIR:$SFSDIR

python3 ./tools/checktests.py --guideline guidelines/next.json
exit_1=$?
python3 ./tools/checktests.py --guideline add-ons/guidelines/dns.next.json --testlib designate_tempest_plugin
exit_2=$?
python3 ./tools/checktests.py --guideline add-ons/guidelines/shared_file_system.next.json --testlib manila_tempest_tests
exit_3=$?
# TODO(kopecmartin) consistency check of heat_tempest_plugin is omitted intentionally until we improve the
# checktests.py so that it detects ids of the heat_tempest_plugin.api tests which don't use decorator.idempotent_id
# call to track the id
# python3 ./tools/checktests.py --guideline add-ons/guidelines/orchestration.next.json --testlib heat_tempest_plugin
# exit_4=$?

python3 ./tools/checktests.py --guideline current_guideline
exit_5=$?
python3 ./tools/checktests.py --guideline add-ons/dns_current_guideline --testlib designate_tempest_plugin
exit_6=$?
python3 ./tools/checktests.py --guideline add-ons/shared_file_system_current_guideline --testlib manila_tempest_tests
exit_7=$?
# python3 ./tools/checktests.py --guideline add-ons/orchestration_current_guideline --testlib heat_tempest_plugin
# exit_8=$?


if [[ "${CLEANTEMPEST}" ]]; then
  rm -rf $TEMPESTDIR
  rm -rf $DNSDIR
  rm -rf $ORCHESTRATIONDIR
  rm -rf $SFSDIR
fi

#! (( $exit_1 || $exit_2 || $exit_3 || $exit_4 || $exit_5 || $exit_6 || $exit_7 || $exit_8 ))
! (( $exit_1 || $exit_2 || $exit_3 || $exit_5 || $exit_6 || $exit_7 ))
