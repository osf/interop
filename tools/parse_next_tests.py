# Copyright (c) 2021 Red Hat, Inc.
# All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import argparse
import json
import os

from refstack.api.guidelines import Guidelines

BASE_URL = "https://opendev.org/api/v1/repos/openinfra/interop/"
REPO_URL = BASE_URL + "contents/guidelines"
RAW_URL = "https://opendev.org/api/v1/repos/openinfra/interop/raw/"
ADDITIONAL_CAPABILITY_URLS = BASE_URL + "contents/add-ons/guidelines"


def parse_arguments():
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument('--out', default="./all_next_tests.txt",
                        help="Name of the file where all next tests will be"
                             "written to.")
    parser.add_argument('--interop-repo', default="./",
                        help="A path to the local interop repository the "
                             "script will look for the next files in.")
    return parser.parse_args()


def parse_tests(f_path, target):
    g = Guidelines(repo_url=REPO_URL, raw_url=RAW_URL,
                   additional_capability_urls=ADDITIONAL_CAPABILITY_URLS)
    f = open(f_path, 'r')
    load_f = json.load(f)
    caps = g.get_target_capabilities(load_f, target=target)
    tests = g.get_test_list(load_f, caps, show_flagged=False)
    return tests


def main():
    args = parse_arguments()
    tests = parse_tests(
        os.path.join(args.interop_repo, 'guidelines/next.json'), 'platform')
    tests += parse_tests(
        os.path.join(args.interop_repo, 'add-ons/guidelines/dns.next.json'),
        'dns')
    tests += parse_tests(
        os.path.join(args.interop_repo,
                     'add-ons/guidelines/orchestration.next.json'),
        'orchestration')
    tests += parse_tests(
        os.path.join(args.interop_repo,
                     'add-ons/guidelines/shared_file_system.next.json'),
        'shared_file_system')
    f = open(args.out, 'w')
    for t in tests:
        f.write(t + "\n")
    f.close()


if __name__ == "__main__":
    main()
